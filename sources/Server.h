#ifndef SERVER_H
#define SERVER_H

#include "constants.h"
#include "ClientTableModel.h"

#include <QMenu>

#include <QMessageBox>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDataStream>
#include <QTime>
#include <QMainWindow>

namespace Ui {
class Server;
}

class Server : public QMainWindow
{
  Q_OBJECT

public:
  explicit Server(int port, QWidget *parent = 0);
  ~Server();

public slots:
  virtual void slotNewConnection();
  void slotReadClient();
  void slotClientDisconnected();
  void slotContextMenuRequested(QPoint position);
  void slotDisconnect();

private:
  Ui::Server *ui;

  QTcpServer *m_tcpServer;
  uint16_t m_nextBlockSize;
  ClientTableModel m_model;

  void sendToClient(QTcpSocket *socket, const QString &str);
};

#endif // SERVER_H
