#include "Server.h"
#include "ui_Server.h"

Server::Server(int port, QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::Server)
{
  ui->setupUi(this);

  ui->tableView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  m_nextBlockSize = 0;

  m_tcpServer = new QTcpServer(this);
  if(!m_tcpServer->listen(QHostAddress::AnyIPv4, port))
  {
    QMessageBox::critical(nullptr,
                          "Ошибка",
                          "Не удалось запустить сервер:"
                          + m_tcpServer->errorString()
                          );
    m_tcpServer->close();
    return;
  }
  connect(m_tcpServer, SIGNAL(newConnection()),
          this, SLOT(slotNewConnection())
          );
  ui->textEdit->append("Сервер запущен.");

  ui->tableView->setModel(&m_model);

  connect(ui->tableView, SIGNAL(customContextMenuRequested(QPoint)),
          this, SLOT(slotContextMenuRequested(QPoint))
          );
}

Server::~Server()
{
  delete m_tcpServer;
  delete ui;
}

/*virtual*/ void Server::slotNewConnection()
{
  QTcpSocket *clientSocket = m_tcpServer->nextPendingConnection();

  QString address = clientSocket->peerAddress().toString();
  QString port = QString::number(clientSocket->peerPort());
  ui->textEdit->append("Установлено новое соединение: " + address + ":" + port);

  connect(clientSocket, SIGNAL(disconnected()),
          clientSocket, SLOT(deleteLater())
          );
  connect(clientSocket, SIGNAL(readyRead()),
          this, SLOT(slotReadClient())
          );
  connect(clientSocket, SIGNAL(disconnected()),
          this, SLOT(slotClientDisconnected())
          );

  m_model.addClient(Client(clientSocket));
}

void Server::slotReadClient()
{
  QTcpSocket *clientSocket = qobject_cast<QTcpSocket*>(sender());
  QDataStream in(clientSocket);
  in.setVersion(QDataStream::Qt_5_7);

  for(;;)
  {
    if(!m_nextBlockSize)
    {
      if(clientSocket->bytesAvailable() < SIZE)
        break;
      in >> m_nextBlockSize;
    }

    if(clientSocket->bytesAvailable() < m_nextBlockSize)
    {
      m_nextBlockSize = 0;
      break;
    }

    QTime time;
    QString str;
    in >> time >> str;

    QString address = clientSocket->peerAddress().toString() + ":" +
                      QString::number(clientSocket->peerPort());

    QString strMessage = time.toString() + "[" + address + "]: " + str;
    ui->textEdit->append(strMessage);

    m_nextBlockSize = 0;

    for(auto x : m_model.getList())
    {
      sendToClient(x.getSocket(), address + " отправил: \"" + str + "\"");
    }
  }
}

void Server::sendToClient(QTcpSocket *socket, const QString &str)
{
  QByteArray block;
  QDataStream out(&block, QIODevice::WriteOnly);
  out.setVersion(QDataStream::Qt_5_7);
  out << static_cast<uint16_t>(0) << QTime::currentTime() << str;

  out.device()->seek(0);
  out << uint16_t(block.size() - SIZE);
  socket->write(block);
}

void Server::slotClientDisconnected()
{
  QTcpSocket *clientSocket = qobject_cast<QTcpSocket*>(sender());
  QString address = clientSocket->peerAddress().toString();
  QString port = QString::number(clientSocket->peerPort());
  ui->textEdit->append(tr("Соединение разорвано: ") + address + ":" + port);

  m_model.removeRow(m_model.findPosition(Client(clientSocket)));
}

void Server::slotContextMenuRequested(QPoint position)
{
  QMenu *menu = new QMenu(this);
  QAction *actionDisconnectClient = new QAction(tr("Отключить"), this);
  connect(actionDisconnectClient, SIGNAL(triggered(bool)), SLOT(slotDisconnect()));
  menu->addAction(actionDisconnectClient);
  menu->popup(ui->tableView->viewport()->mapToGlobal(position));
}

void Server::slotDisconnect()
{
  int row = ui->tableView->selectionModel()->currentIndex().row();
  if(row >= 0)
  {
    m_model.at(row).getSocket()->disconnectFromHost();
  }
}
