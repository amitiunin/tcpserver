#include "Server.h"
#include <QApplication>
#include <QDebug>
#include <QMessageBox>

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  QStringList args = QApplication::arguments();
  if (args.size() == 1)
    return 400;

  bool ok;
  int port = args.at(1).toInt(&ok);
  if(!ok)
    return 400;

  Server *server = new Server(port);
  server->show();

  return app.exec();
}
