#include "ClientTableModel.h"

ClientTableModel::ClientTableModel(QObject *parent)
  : QAbstractTableModel(parent)
{
}

QVariant ClientTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(role != Qt::DisplayRole)
    return QVariant();

  if (orientation == Qt::Horizontal)
    switch(section)
    {
      case 0:
        return tr("IP");
        break;

      case 1:
        return tr("Порт");
        break;
    }

  return QVariant();
}

int ClientTableModel::rowCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);
  return list.size();
}

int ClientTableModel::columnCount(const QModelIndex &parent) const
{
  Q_UNUSED(parent);
  return 2;
}

QVariant ClientTableModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid())
    return QVariant();

  if(index.row() >= list.size() || index.row() < 0)
    return QVariant();

  if(role == Qt::DisplayRole)
  {
    Client client = list.at(index.row());
    switch(index.column())
    {
      case 0:
        return QVariant(client.getAddress());
        break;

      case 1:
        return QVariant(client.getPort());
        break;
    }
  }

  return QVariant();
}

Qt::ItemFlags ClientTableModel::flags(const QModelIndex &index) const
{
  if(!index.isValid())
    return Qt::ItemIsEnabled;

  return QAbstractTableModel::flags(index) | Qt::ItemIsEnabled; // FIXME: Implement me!
}

bool ClientTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
  beginInsertRows(parent, row, row + count - 1);

  endInsertRows();
  return true;
}

void ClientTableModel::addClient(Client client)
{
  insertRow(rowCount());
  list.push_back(client);
}

bool ClientTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
  beginRemoveRows(parent, row, row + count - 1);

  for(int i = 0; i < count; ++i)
    list.removeAt(row);

  endRemoveRows();
  return true;
}

uint ClientTableModel::findPosition(Client client)
{
  return static_cast<uint>(list.indexOf(client));
}

const Client ClientTableModel::at(int pos)
{
  if(pos < list.size())
    return list.at(pos);
  else
    return Client();
}

const QList<Client>& ClientTableModel::getList() const
{
  return list;
}
