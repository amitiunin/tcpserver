#ifndef CLIENTTABLEMODEL_H
#define CLIENTTABLEMODEL_H

#include "Client.h"

#include <QHostAddress>
#include <QTcpSocket>
#include <QList>
#include <QAbstractTableModel>

class ClientTableModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  explicit ClientTableModel(QObject *parent = 0);

  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
//  bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

  bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
  void addClient(Client client);

  bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

  uint findPosition(Client client);

  const Client at(int pos);

  const QList<Client> &getList() const;

private:
  QList<Client> list;
};

#endif // CLIENTTABLEMODEL_H
