#include "Client.h"

Client::Client(QTcpSocket *socket)
{
  setSocket(socket);
}

Client::Client(const Client &other)
{
  m_socket = new QTcpSocket(other.getSocket());
  m_address = other.m_address;
  m_port = other.m_port;
}

bool Client::operator ==(const Client &other) const
{
  return (m_socket == other.m_socket);
}

void Client::setSocket(QTcpSocket *socket)
{
  m_socket = socket;
  if(socket)
  {
    m_address = m_socket->peerAddress().toString();
    m_port = m_socket->peerPort();
  }
}

QTcpSocket* Client::getSocket() const
{
  return m_socket;
}

QString Client::getAddress() const
{
  return m_address;
}

uint16_t Client::getPort() const
{
  return m_port;
}
