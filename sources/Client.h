#ifndef CLIENT_H
#define CLIENT_H

#include <QHostAddress>
#include <QTcpSocket>

class Client
{
public:
  Client(QTcpSocket *socket = nullptr);
  Client(const Client &other);

  bool operator ==(const Client &other) const;

  void setSocket(QTcpSocket *socket);

  QTcpSocket* getSocket() const;
  QString getAddress() const;
  uint16_t getPort() const;

private:
  QTcpSocket *m_socket;
  QString m_address;
  uint16_t m_port;
};

#endif // CLIENT_H
