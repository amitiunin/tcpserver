#-------------------------------------------------
#
# Project created by QtCreator 2016-09-05T20:31:02
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Server
TEMPLATE = app

HEADERS += \
    sources/Client.h \
    sources/ClientTableModel.h \
    sources/Server.h \
    sources/constants.h

SOURCES += \
    sources/Client.cpp \
    sources/ClientTableModel.cpp \
    sources/main.cpp \
    sources/Server.cpp

FORMS    += sources/Server.ui
